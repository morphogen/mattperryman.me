---
title: "Subscribed"
headline: "Hol up partner!"
description: |
  You ain't done yet.
date: 2021-04-25T11:13:48+12:00
tags: [ "" ]
layout: squeeze
draft: false
---

Your email address is confirmed. You'll begin receving emails from me, at the address:

**matt@mattperryman.com**

> **Pro-Tip:** To make sure you receive emails from me, add that address to your contacts list. 


