---
title: "About Me"
description: |
  What I do (and what I can do for YOU)
date: 2021-04-12T12:10:01+12:00
draft: false
---

*Looking to contact me about your project? Go here: [Contact]({{< ref "contact" >}})*

*Are you here for my current projects & thinking? Go here: [Now]({{< ref "now" >}})*

Hey there. Matt Perryman here.

...

You can get that here:

https://mattperryman.me/offer

