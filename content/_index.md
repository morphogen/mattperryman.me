---
title: "If you've got (EASY THING), then I can help you (SOLVE MAJOR PROBLEM) – *even if (MAJOR OBSTACLE)*"
date: 2021-04-12T12:07:49+12:00
description: |
  Wonder why you aren't getting the results you want? Read on...
draft: false

---

Opening sentence – qualify and dramatic tension.

**Here's the story:** A Character, with a Problem, in a Setting

Agitate that problem. 

Wouldn't it be nice if...? 

Paint a picture of a perfect life after solving this problem...

Let somebody else tell it (Testimonial)

### Copywriting →

What is it?

Explain the "how" of the mechanism and the process behind it.

Explain the "why", that is, why it works... and why it will *work for them* (even if...)

[Read more about Copywriting Services &rarr;]({{< ref "copywriting" >}})

### Process design →

What is it?

Explain the "how" of the mechanism and the process behind it.

Explain the "why", that is, why it works... and why it will *work for them* (even if...)

[Read more about Process Design &rarr;]({{< ref "process" >}})

### Uniqueness Consulting →

What is it?

Explain the "how" of the mechanism and the process behind it.

Explain the "why", that is, why it works... and why it will *work for them* (even if...)

[Read more about Uniqueness Consulting &rarr;]({{< ref "unique" >}})

**Here's what you're going to get:** Tell them exactly what's in the product and what they'll get when they order. 

What's it going to cost? Reframe the price and put it in comparative terms. COST? This is an investment! 

Remind them that...

## Ordering is Simple, Discreet & ANONYMOUS

**Here's how to get it:** Tell them exactly what to do to buy.

And you'll get these GREAT BONUSES

Then TAKE AWAY THE CARROT. There's only a limited number/limited time/I'm a total jerk who won't let you have it. 

You can do (this old way)... or you can do (this new thing that will bring you satisfaction)

Best act now. Click here to buy:

JOIN/BUY NOW BUTTON

Sincerely,

Matt Perryman

**P.S.** Add another bonus or twist the knife even more.

