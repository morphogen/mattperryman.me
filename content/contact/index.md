---
title: "Contact"
description: |
  How to work with Matt Perryman on your next project
date: 2021-04-12T12:10:13+12:00
draft: false
---

The easiest way to get in touch is by email:

**matt (at) mattperryman (dot) com**

That will bring you straight to my inbox. 

I don't do social media, if you were hoping for a Facebook Messenger or Twitter DM. 

If you want to try, here's the best options in order from most to least effective:

* [Telegram Channel](https://t.me/mpdotcom "Telegram")

* [LinkedIn Profile](https://www.linkedin.com/in/multifarious/ "LinkedIn profile")

