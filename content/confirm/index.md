---
title: "Confirm"
headline: "Confirm your email address"
description: |
  
date: 2021-04-25T11:13:37+12:00
tags: [ "" ]
layout: squeeze
draft: true
---

You'll need to confirm your email address before I can send you the emails I promised.
