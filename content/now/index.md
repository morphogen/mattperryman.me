---
title: "Now"
description: |
  Current projects & thinking as of Now
date: 2021-04-12T12:10:09+12:00
where: "Auckland, New Zealand"
draft: false
---


## April 2021

Designing this website for my new consulting business.

...

If you want to check that out, go here:

https://mattperryman.me/offer


*This is a [Now Page](https://nownownow.com/about) inspired by Derek Sivers.*